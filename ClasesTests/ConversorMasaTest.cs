﻿using ConversionUnidades.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ConversionUnidadesTest.ClasesTests
{
    public class ConversorMasaTest
    {
        [Fact]
        public void ConvertirKilogramosALibrasTest()
        {
            // Arrange
            var conversor = new ConversorMasa();

            // Act
            double resultado = conversor.ConvertirKilogramosALibras(2.5);

            // Assert
            Assert.Equal(5.51, resultado);
        }

        [Fact]
        public void ConvertirToneladaAKilogramos()
        {
            // Arrange
            var conversor = new ConversorMasa();

            // Act
            double resultado = conversor.ConvertirToneladaAKilogramos(8);

            // Assert
            Assert.Equal(8000, resultado);
        }

        [Fact]
        public void ConvertirLibrasAGramosTest()
        {
            // Arrange
            var conversor = new ConversorMasa();

            // Act
            double resultado = conversor.ConvertirLibrasAGramos(12);

            // Assert
            Assert.Equal(5443.08, resultado);
        }
    }
}
