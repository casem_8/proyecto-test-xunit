﻿using ConversionUnidades.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ConversionUnidadesTest.ClasesTests
{
    public class ConversorTemperaturaTest
    {
        [Fact]
        public void CelsiusAFahrenheitTest()
        {
            // Arrange
            var conversor = new ConversorTemperatura();

            // Act
            int resultado = conversor.CelsiusAFahrenheit(25);

            // Assert
            Assert.Equal(77, resultado);
        }

        [Fact]
        public void FahrenheitACelsiusTest()
        {
            // Arrange
            var conversor = new ConversorTemperatura();

            // Act
            int resultado = conversor.FahrenheitACelsius(77);

            // Assert
            Assert.Equal(25, resultado);
        }
    }
}
