﻿using ConversionUnidades.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ConversionUnidadesTest.ClasesTests
{
    public class ConversoLongitudTests
    {
        [Fact]
        public void ConvertirMetrosAKilometrosTest()
        {
            // Arrange
            var conversor = new ConversorLongitud();

            // Act
            double resultado = conversor.ConvertirMetrosAKilometros(5450);

            // Assert
            Assert.Equal(5.45, resultado); 
        }

        [Fact]
        public void ConvertirCentimetrosAPulgadasTest()
        {
            // Arrange
            var conversor = new ConversorLongitud();

            // Act
            double resultado = conversor.ConvertirCentimetrosAPulgadas(30);

            // Assert
            Assert.Equal(11.81, resultado);
        }

        [Fact]
        public void ConvertirKilometrosACentimetrosTest()
        {
            // Arrange
            var conversor = new ConversorLongitud();

            // Act
            double resultado = conversor.ConvertirKilometrosACentimetros(3.5);

            // Assert
            Assert.Equal(350000, resultado);
        }
    }
}
